import numpy as np
from crayflow import data

def test_normalize():
  d = np.ones(shape=(12, 11))
  d1, = data.normalize(d, inline=False)(d)

  assert not np.allclose(d, d1)
  assert d is not d1

  d2, = data.normalize(d, inline=True)(d)
  assert np.allclose(d, d2)
  assert d is d2

  d3, = data.normalize(d, inline=True, dtype='float32')(d)
  assert np.allclose(d, d2)
  assert d is not d3

  d = np.ones(shape=(12, 11), dtype='uint8')
  d4, = data.normalize(d, inline=True)(d)
  assert not np.allclose(d, d4)
  assert d is not d4
  assert d4.dtype == np.float32

  d = np.ones(shape=(12, 11), dtype='uint8')
  d5, = data.normalize(d, inline=True, dtype='float128')(d)
  assert np.allclose(d4, d5)
  assert d is not d5
  assert d5.dtype == np.float128