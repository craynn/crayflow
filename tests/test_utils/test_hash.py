import inspect
import numpy as np

def get_func(X):
  url = 'asasa'
  z = {1: 2, 3: 4}, list()

  class A(object):
    def __init__(self, j):
      self.j = np.array(j)

    def __call__(self, x):
      return x + self.j

  a1 = A(2)
  a2 = A(X)
  a3 = A(1000)

  class B(object):
    def __init__(self, f):
      self.f = f

    def __call__(self, x):
      return self.f(x) + 2

  b = B(a1)

  def g(x):
    return x + b(x)

  def f(x):
    return x + g(x) + url + a2(x)

  return f

def get_alt_func(X):
  url = 'asasa'
  z = {1: 2, 3: 4}, list()

  class A(object):
    def __init__(self, j):
      self.j = np.array(j)

    def __call__(self, x):
      return x + self.j

  a1 = A(2)
  a2 = A(X)
  a3 = A(1000)

  class B(object):
    def __init__(self, f):
      self.f = f
      self.z = z

    def __call__(self, x):
      return self.f(x) + 2

  b = B(a1)

  def g(x):
    return x + b(x)

  def f(x):
    return x + g(x) + url + a2(x)

  return f

def test_dependencies():
  from crayflow.utils import get_function_hash
  assert get_function_hash(get_func(1)) == get_function_hash(get_func(1))
  assert get_function_hash(get_func(1)) != get_function_hash(get_func(2))
  assert get_function_hash(get_func(1)) != get_function_hash(get_alt_func(1))
  assert get_function_hash(get_alt_func(1)) == get_function_hash(get_alt_func(1))
  print()
  print(get_function_hash(get_alt_func(1)))
