import os
import tempfile
import numpy as np

def test_npz():
  from crayflow import cache

  with tempfile.TemporaryDirectory() as dir:
    c = cache.npz_cache(os.path.join(dir, 'tmp.npz'), 'x', 'y')
    x = np.random.uniform(size=(10, 12))
    y = np.random.uniform(size=(13, 9))
    c.save([x, y])
    x_, y_ = c.load()

    assert np.all(np.equal(x, x_))
    assert np.all(np.equal(y, y_))
