from crayflow import dataflow, for_each, stage
from crayflow.meta import Cache, Stage

def test_dataflow():
  class Origin(Stage):
    def __init__(self):
      self.x = None
      super(Origin, self).__init__()

    def load(self, root=None):
      raise FileNotFoundError()

    def get_output_for(self, root=None, warn=False):
      return 1

  origin = lambda : Origin

  class DictCache(Cache):
    def __init__(self, key, storage):
      self.key = key
      self.storage = storage

    def load(self, root=None):
      return self.storage[self.key]

    def save(self, obj, root=None):
      if self.key in self.storage:
        raise Exception('Attempt to rewrite cached value!')
      else:
        self.storage[self.key] = obj

  def dict_storage():
    storage = dict()

    def f(key):
      return DictCache(key, storage)

    return storage, f

  storage, dict_cache = dict_storage()

  class trap(object):
    def __init__(self, f):
      self.f = f
      self.computed = False

    def __call__(self, *args, **kwargs):
      if self.computed:
        raise Exception('Repeated computations!')
      else:
        self.computed = True
        return self.f(*args)

  def subflow1(*incoming):
    return stage()(
      trap(lambda x, y: x + y)
    )(*incoming)

  subflow2 = (
    stage(cache=dict_cache('z'))(
      trap(lambda x, y: x + y)
    ),
    stage()(
      lambda x: x - 1
    )
  )

  flow = dataflow(
    [
      origin(),
      stage()(trap(lambda : 1))
    ],
    for_each(
      stage()(lambda x: x * 3)
    ),
    subflow1,
    stage()(
      trap(lambda x : x + 3)
    ) @ dict_cache('y'),
    [
      stage()(
        trap(lambda x: x // 2),
      ),

      stage()(
        trap(lambda x: x + 2)
      )
    ],
    subflow2
  )

  print()
  print(flow(warn=True))
  print(flow(warn=True))
  print(flow(warn=True))

  assert storage == dict(y=9, z=15)