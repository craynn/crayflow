from crayflow import configurable

def test_dataflow():
  flow = configurable.dataflow(
    configurable.stage(name='origin')(
      lambda x: x
    ),

    configurable.stage(name='addition')(
      lambda x, inc=1: x + inc
    )
  )

  assert flow(origin=dict(x=1), addition=dict(inc=12))

  try:
    assert flow(origin=dict(x=1), addition=dict(inc=12, y=1))
    raise Exception()
  except TypeError:
    pass
