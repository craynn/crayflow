from crayflow import dataflow, for_each, stage
from crayflow.meta import Cache, Stage
from crayflow import configurable

def test_dataflow():
  def double(x, y, z):
    return (
      x * y - z,
      x + y * z,
    )

  flow = configurable.dataflow(
    [
      stage(name='origin')(lambda: (1, 2)),
      stage(name='origin')(lambda: 3),
    ],

    [
      stage(name='addition')(lambda x, y, z: x + y + z),
      stage(name='OTHER')(double),
    ]
  )

  assert flow() == (6, -1, 7)