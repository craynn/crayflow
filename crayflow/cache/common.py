import os

from ..meta import Cache
from ..sources.utils import *

__all__ = [
  'pickled',
  'npz_cache',
  'npy_cache',
  'no_cache'
]

class PickleCache(Cache):
  def __init__(self, path):
    self.path = path

  def save(self, obj):
    ensure_directory(os.path.dirname(self.path))

    with open(self.path, 'wb') as f:
      import pickle
      pickle.dump(obj, f)

  def load(self):
    with open(self.path, 'rb') as f:
      import pickle
      return pickle.load(f)

class NPZCache(Cache):
  def __init__(self, path, *names):
    self.path = path
    self.names = names

  def save(self, obj):
    import numpy as np
    np.savez(self.path, **dict(zip(self.names, obj)))

  def load(self):
    import numpy as np

    with np.load(self.path) as f:
      return tuple([f[name] for name in self.names])

class NPYCache(Cache):
  def __init__(self, path):
    self.path = path

  def save(self, obj):
    import numpy as np
    np.save(self.path, obj, allow_pickle=False)

  def load(self, root=None):
    import numpy as np
    return np.load(self.path, allow_pickle=False, )

pickled = PickleCache
npz_cache = NPZCache
npy_cache = NPYCache

from ..meta import NoCache
no_cache = NoCache