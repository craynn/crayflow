from .mnist import *
from .cifar import *
from .svhn import *
from .omniglot import *

from .celebA import *

from .higgs import *
from .kdd_99 import *

from .amazon_reviews import *